package PastebinPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PastebinHomePage {
    private WebDriver driver;
    @FindBy(id = "postform-text")
    private WebElement searchInput;
    @FindBy(xpath = "//*[@id='w0']/div[5]/div[1]/div[3]/div/span/span[1]/span")
    private WebElement syntaxHighlighting;
    @FindBy(xpath = "//*[@id='w0']/div[5]/div[1]/div[4]/div/span/span[1]/span")
    private WebElement pasteExpiration;
    @FindBy(id = "postform-name")
    private WebElement title;
    @FindBy(xpath = "//button[@type='submit']")
    private WebElement create;

    public PastebinHomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void writeCode(String text) {
        this.searchInput.sendKeys(text);
    }

    public void setSyntaxHighlighting(String syntaxPath) {
        this.clickOptionByXpath(this.syntaxHighlighting, syntaxPath);
    }

    public void setPasteExpiration(String expirationPath) {
        this.clickOptionByXpath(this.pasteExpiration, expirationPath);
    }

    public void setTitle(String pasteName) {
        title.sendKeys(pasteName);
    }

    public void createNewPaste() {
        this.create.click();
    }

    public void clickOptionByXpath(WebElement name, String path) {
        name.click();
        name = this.driver.findElement(By.xpath(path));
        name.click();
    }
}
