package PastebinPages;

import java.util.concurrent.TimeUnit;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
public class pastebinRun {
    WebDriver driver;
    PastebinHomePage homePage;


    @BeforeTest
    public void setUp() {
        ChromeOptions handlingssl = new ChromeOptions();
        handlingssl.setAcceptInsecureCerts(true);
        this.driver = new ChromeDriver(handlingssl);
        this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        this.driver.get("https://pastebin.com");
    }

    @Test
    public void test1() {
        this.homePage = new PastebinHomePage(this.driver);
        this.homePage.writeCode("git config --global user.name  'New Sheriff in Town'\\ngit reset $(git commit-tree HEAD^{tree} -m 'Legacy code')\\ngit push origin master --force");
        this.homePage.setSyntaxHighlighting("//li[@class='select2-results__option' and text()='Bash']");
        this.homePage.setPasteExpiration("//li[text()='10 Minutes']");
        this.homePage.setTitle("how to gain dominance among developers");
        this.homePage.createNewPaste();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(this.driver.getTitle(), "how to gain dominance among developers", "title does not match");
        WebElement syntax = this.driver.findElement(By.xpath("//a[@href='/archive/csharp' and @class='btn -small h_800']"));
        String syntaxText = syntax.getText();
        softAssert.assertEquals("Bash", syntaxText);
        WebElement code = this.driver.findElement(By.xpath("//*[@class='de1']"));
        String codeText = code.getText();
        softAssert.assertEquals("git config --global user.name  'New Sheriff in Town'\\ngit reset $(git commit-tree HEAD^{tree} -m 'Legacy code')\\ngit push origin master --force", codeText);
        softAssert.assertAll();
    }
}
